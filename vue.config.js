// Indicates that we are building in the Gitlab CI environment and therefore the app is live
//const isCiBuild = !!process.env.CI;

module.exports = {
  //publicPath: isCiBuild ? '/chord-progress-vue/' : '/',

  chainWebpack: (config) => {
    const svgRule = config.module.rule('svg');
 
    svgRule.uses.clear();
 
    svgRule
      .use('babel-loader')
      .loader('babel-loader')
      .end()
      .use('vue-svg-loader')
      .loader('vue-svg-loader');
  },

  pwa: {
    name: 'Chordfish',
    themeColor: '#0B69A3',
    msTileColor: '#B3ECFF',
    manifestOptions: {
      'start_url': '/index.html',
      'background_color': '#F7F7F7'
    }
  }
};