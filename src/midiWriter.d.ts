type pitch = string | number | string[] | number[]
type duration = string | number | string[] | number[]

interface NoteEventOptions {
  pitch: pitch
  duration: duration
  wait?: duration
  sequential?: boolean
  velocity?: number
  repeat?: number
  channel?: number
  grace?: pitch
  startTick?: number
}

declare module 'midi-writer-js' {
  declare class NoteEvent {
    constructor (options: NoteEventOptions)
  }

  declare class Track {
    addEvent(event, mapFunction?)
    setTempo(tempo: number)
    addText(text: string)
    addCopyright(text: string)
    addTrackName(text: string)
    addInstrumentName(text: string)
    addMarker(text: string)
    addCuePoint(text: string)
    addLyric(text: string)
    setTimeSignature(numerator: number, denominator: number)
  }

  declare class ProgramChangeEvent {
    constructor (options)
  }

  declare class Writer {
    constructor (tracks)
    buildFile(): number[]
    base64(): string
    dataUri(): string
    stdout()
  }
}