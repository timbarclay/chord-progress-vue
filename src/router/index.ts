import Vue from "vue"
import VueRouter, { RawLocation, RouteConfig } from "vue-router"
import Create from "../views/Create.vue"
import NotFound from '@/views/NotFound.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Create",
    component: Create
  },
  {
    path: "/index.html",
    component: Create
  },
  {
    path: "/ear-training",
    name: "Training",
    component: () => import(/* webpackChunkName: "training" */'../views/Training.vue')
  },
  {
    path: "/ear-test",
    name: "Testing",
    component: () => import(/* webpackChunkName: "testing" */'../views/Testing.vue')
  },
  {
    path: "/practice",
    name: "Practice",
    component: () => import(/* webpackChunkName: "practice" */'../views/Practice.vue'),
    redirect: { name: 'Practice scale' },
    children: [
      {
        path: "scale",
        name: "Practice scale",
        component: () => import('../views/PracticeScale.vue')
      },
      {
        path: "chord",
        name: "Practice chord",
        component: () => import('../views/PracticeChord.vue')
      }
    ]
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: '*',
    name: 'NotFound',
    component: NotFound
  }
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const fromIsFlats = from.query.isFlats
  const toIsFlats = to.query.isFlats
  if (fromIsFlats && toIsFlats === undefined) {
    const query = Object.assign({}, to.query, { isFlats: fromIsFlats })
    const newTo = Object.assign({}, to, { query }) as RawLocation
    next(newTo)
  } else {
    return next()
  }
})

export default router;
