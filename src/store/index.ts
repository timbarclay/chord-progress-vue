import sampler, { SamplerState } from './modules/sampler'
import chordProgression, { ChordProgressionState } from './modules/chordProgression'
import app, { AppState } from './modules/app'

import Vue from "vue";
import Vuex, { StoreOptions } from "vuex";
import VuexPersistence from 'vuex-persist'

export interface FullState {
  sampler: SamplerState,
  chordProgression: ChordProgressionState,
  app: AppState
}

const vuexLocal = new VuexPersistence<FullState>({
  storage: window.localStorage,
  key: 'chord-cheat',
  reducer (state) {
    return {
      app: state.app
    }
  }
})

Vue.use(Vuex);

const storeOptions: StoreOptions<FullState> = {
  modules: {
    sampler,
    chordProgression,
    app
  },
  plugins: [vuexLocal.plugin]
}

const store = new Vuex.Store(storeOptions)

store.dispatch('sampler/initSampler', 'piano')

export default store