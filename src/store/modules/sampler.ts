import { Module, ActionTree, MutationTree } from 'vuex'
import * as Tone from 'tone'
import {SampleLibrary, instruments} from '@/application/SampleLoader'
import _pick from 'lodash/pick'

export interface SamplerState {
  instrument?: string,
  samplerLoaded: boolean,
  sampler?: Tone.Sampler
}

const defaultState = {
  samplerLoaded: false
} as SamplerState

const mutations: MutationTree<SamplerState> = {
  setInstrument(state, instrument: string) {
    state.instrument = instrument
  },
  setSamplerLoaded(state, loaded: boolean) {
    state.samplerLoaded = loaded
  },
  setSampler(state, sampler: Tone.Sampler) {
    state.sampler = sampler
  }
}

const actions: ActionTree<SamplerState, {}> = {
  initSampler(context, instrument: string): void {
    if(!instruments.includes(instrument)) {
      console.error(`Instrument ${instrument} not recognised`)
      return
    }

    context.commit('setInstrument', instrument)

    const sampler = new Tone.Sampler(SampleLibrary[instrument], () => {
      context.commit('setSamplerLoaded', true)
    }, `${process.env.BASE_URL}samples/${instrument}/`)
      .toDestination()
    
    context.commit('setSampler', sampler)
  }
}

export default {
  namespaced: true,
  actions,
  mutations,
  state: defaultState
} as Module<SamplerState, {}>

export function samplerReducer (state: SamplerState) {
  return _pick(state, ['instrument'])
}