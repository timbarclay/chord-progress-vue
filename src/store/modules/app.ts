import { MutationTree, ActionTree, Module } from 'vuex'

export interface AppState {
  bannerDismissed: boolean
  keyBindingsActive: boolean
}

const defaultState = {
  bannerDismissed: false,
  keyBindingsActive: true
} as AppState

const mutations: MutationTree<AppState> = {
  setBannerDismissed(state, dismissed: boolean) {
    state.bannerDismissed = dismissed
  },
  setKeyBindingsActive(state, active: boolean) {
    state.keyBindingsActive = active
  }
}

const actions: ActionTree<AppState, {}> = {
  setBannerDismissed(context, dismissed: boolean) {
    context.commit('setBannerDismissed', dismissed)
  },
  setKeyBindingsActive(context, active: boolean) {
    context.commit('setKeyBindingsActive', active)
  }
}

export default {
  namespaced: true,
  actions,
  mutations,
  state: defaultState
} as Module<AppState, {}>