import { Module, ActionTree, MutationTree } from 'vuex'

export interface ChordProgressionState {
  selectedIndex?: number
  progressionName: string
}

const defaultState = {
  selectedIndex: undefined,
  progressionName: 'My chord progression'
} as ChordProgressionState

const mutations: MutationTree<ChordProgressionState> = {
  selectIndex(state, index: number) {
    state.selectedIndex = index
  },
  deselectIndex(state, index: number) {
    if (state.selectedIndex === index) {
      state.selectedIndex = undefined
    }
  },
  setName(state, progressionName: string) {
    state.progressionName = progressionName
  }
}

const actions: ActionTree<ChordProgressionState, {}> = {
  selectIndex(context, index: number) {
    context.commit('selectIndex', index)
  },
  deselectIndex(context, index: number) {
    context.commit('deselectIndex', index)
  },
  setName(context, progressionName: string) {
    context.commit('setName', progressionName)
  }
}

export default {
  namespaced: true,
  actions,
  mutations,
  state: defaultState
} as Module<ChordProgressionState, {}>