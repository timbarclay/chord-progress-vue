import Vue, { VueConstructor } from 'vue'
import { ChordProgression } from '@/application/models/chordProgression'
import { getChordProgression, parseEditFromQuery } from '@/application/ChordProgressionManager'
import hasScaleInKey, { HasScaleInKeyMixinData } from '@/mixins/hasScaleInKey'

export interface HasChordProgressionMixinData extends Vue {
  chordProgression: ChordProgression
  edit: boolean
}

export default (Vue as VueConstructor<HasScaleInKeyMixinData>).extend({
  mixins: [hasScaleInKey],
  data() {
    return {
      chordProgression: new ChordProgression([], this.scaleInKey), // this is just here for the typing. It get's reassigned immediately
      edit: true
    }
  },
  watch: {
    '$route.query': {
      handler() {
        this.chordProgression = getChordProgression(this.$route, this.scaleInKey)
        this.edit = parseEditFromQuery(this.$route)
      },
      immediate: true
    }
  }
})