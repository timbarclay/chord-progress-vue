import Vue from 'vue'

export interface HasBreakpointsMixinComputed extends Vue {
  isMobile: boolean
}

export default Vue.extend({
  computed: {
    isMobile(): boolean {
      return this.$screen.breakpoint === 'mobile'
    }
  }
})