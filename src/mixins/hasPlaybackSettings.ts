import Vue from 'vue'
import { getBpm, getLoop, getArpeggio } from '@/application/PlaybackSettingsManager'

export interface HasPlaybackSettingsMixin extends Vue {
  bpm: number
  loop: boolean
  arpeggio: boolean
}

export default Vue.extend({
  data() {
    return {
      loop: true,
      arpeggio: false,
      bpm: 200 // just here for the typing
    }
  },
  watch: {
    '$route.query': {
      handler() {
        this.loop = getLoop(this.$route)
        this.arpeggio = getArpeggio(this.$route)
        this.bpm = getBpm(this.$route)
      },
      immediate: true
    }
  }
})