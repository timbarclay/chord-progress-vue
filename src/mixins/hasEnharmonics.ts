import Vue from 'vue'
import { getIsFlats } from '@/application/EnharmonicManager'

export interface HasEnharmonicsMixinData extends Vue {
  isFlats: boolean
}

export default Vue.extend({
  data() {
    return {
      isFlats: false
    }
  },
  watch: {
    '$route.query': {
      handler() {
        this.isFlats = getIsFlats(this.$route)
      },
      immediate: true
    }
  }
})