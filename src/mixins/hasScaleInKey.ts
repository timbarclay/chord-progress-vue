import Vue from 'vue'
import { ScaleInKey } from '@/application/models/ScaleInKey'
import { majorScale, Scale } from '@/application/models/scales'
import { getKeyNote, getKeyScale } from '@/application/KeyManager'
import { notes, Note } from '@/application/models/notes'

export interface HasScaleInKeyMixinData extends Vue {
  note: Note
  scale: Scale
  scaleInKey: ScaleInKey
}

export default Vue.extend({
  data() {
    return {
      selectedNote: notes[0],
      selectedScale: majorScale
    }
  },
  computed: {
    scaleInKey(): ScaleInKey {
      return new ScaleInKey(this.selectedNote, this.selectedScale)
    }
  },
  watch: {
    '$route.query': {
      handler() {
        this.selectedNote = getKeyNote(this.$route)
        this.selectedScale = getKeyScale(this.$route)
      },
      immediate: true
    }
  }
})