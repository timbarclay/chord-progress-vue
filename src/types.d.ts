declare module '*.svg?inline' {
  const content: any
  export default content
}

declare module '*.svg' {
  const content: any
  export default content
}

declare module 'v-hotkey'

type Keymap = {[key: string]: (event: KeyboardEvent) => void}