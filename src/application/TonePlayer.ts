import { Sampler, Frequency, now, Transport, Time } from 'tone'
import { KeyScaleChord, ScaleInKey } from './models/ScaleInKey'
import { ChordProgression } from './models/chordProgression'

function getFrequencies(chord: KeyScaleChord, inversion: number, startOctave: number) {
  return chord.midiNotes(startOctave, inversion)
    .map(n => Frequency(n, 'midi').toNote())
}

export function playChord(instrument: Sampler, chord: KeyScaleChord, inversion: number, duration: string, startOctave: number, start?: number): void {
  const freqs = getFrequencies(chord, inversion, startOctave)
  instrument.triggerAttackRelease(freqs, duration, start || now(), 0.4)
}

export function playArpeggio(instrument: Sampler, chord: KeyScaleChord, inversion: number, duration: string, startOctave: number, start?: number): void {
  const begin = Time(start || now()).toSeconds()
  const length = Time('4n').toSeconds()
  const freqs = getFrequencies(chord, inversion, startOctave)
  // Make sure we have 4 notes to play per bar
  const freqs4 = freqs.length === 3 ? [...freqs, freqs[0]] : freqs
  freqs4.forEach((freq, i) => {
    instrument.triggerAttackRelease([freq], duration, begin + (length * i), 0.4)
  })
}

export function queueProgression(instrument: Sampler, progression: ChordProgression, scale: ScaleInKey, arpeggio: boolean, finishedCallback?: () => void) {
  Transport.loopEnd = progression.chords.length + 'm'
  Transport.cancel()
  progression.chords.forEach((chord, i) => {
    Transport.schedule(time => {
      if (chord) {
        if (arpeggio) {
          playArpeggio(instrument, chord.getChord(scale, false), chord.inversion, '2n', chord.octave, time)
        } else {
          playChord(instrument, chord.getChord(scale, false), chord.inversion, '1m', chord.octave, time)
        }
      }
      if (i === progression.chords.length - 1 && finishedCallback) {
        finishedCallback()
      }
    }, i + 'm')
  })
}