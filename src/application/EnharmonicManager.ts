import VueRouter, { Route } from 'vue-router'
import { saveState } from './managerHelpers';

/**
 * Refers to whether black note scale roots should be referred to as sharps or flats. E.g. if isFlats=true use Db; if it's false (or absent) use C#
 */
interface EnharmonicQuery {
  isFlats?: boolean
}

export function getIsFlats(route: Route): boolean {
  const { isFlats } = route.query as EnharmonicQuery
  return !!isFlats
}

export function setIsFlats(route: Route, router: VueRouter, isFlats: boolean): void {
  const query = Object.assign({}, route.query, {
    isFlats
  })
  saveState(query, route.query, route.path, router)
}