import VueRouter, { Route } from 'vue-router'
import { saveState } from './managerHelpers'

interface PlaybackSettingsQuery {
  bpm?: string
  loop?: string
  arpeggio?: string
}

const DEFAULT_BPM = 200
const MAX_BPM = 400
const MIN_BPM = 1
const DEFAULT_LOOP = true
const DEFAULT_ARPEGGIO = false

export function getBpm(route: Route): number {
  const { bpm } = route.query as PlaybackSettingsQuery
  // This weirdness is to handle both the case where it's falsy and where it's not falsy but coerces to NaN
  const bpmNum = Math.abs(+(bpm || DEFAULT_BPM) || DEFAULT_BPM)
  if (bpmNum > MAX_BPM) return MAX_BPM
  if (bpmNum < MIN_BPM) return MIN_BPM
  return bpmNum
}

export function getLoop(route: Route): boolean {
  const { loop } = route.query as PlaybackSettingsQuery
  if (typeof loop === 'boolean') return loop
  if (typeof loop === 'string') return loop === 'true'
  return DEFAULT_LOOP
}

export function getArpeggio(route: Route): boolean {
  const { arpeggio } = route.query as PlaybackSettingsQuery
  if (typeof arpeggio === 'boolean') return arpeggio
  if (typeof arpeggio === 'string') return arpeggio === 'true'
  return DEFAULT_ARPEGGIO
}

export function setBpm(route: Route, router: VueRouter, bpm: number): void {
  const query = Object.assign({}, route.query, {
    bpm: bpm || DEFAULT_BPM
  })
  saveState(query, route.query, route.path, router)
}

export function setLoop(route: Route, router: VueRouter, loop: boolean): void {
  const query = Object.assign({}, route.query, {
    loop
  })
  saveState(query, route.query, route.path, router)
}

export function setArpeggio(route: Route, router: VueRouter, arpeggio: boolean): void {
  const query = Object.assign({}, route.query, {
    arpeggio
  })
  saveState(query, route.query, route.path, router)
}