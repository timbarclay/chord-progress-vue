import VueRouter, { Route } from 'vue-router'
import { ChordProgressionChord, SetChordParameters, ChordProgression } from './models/chordProgression';
import { ScaleInKey } from './models/ScaleInKey';
import { saveState } from './managerHelpers';

interface ChordProgressionQuery {
  chords?: string
  edit?: boolean | string
}

const CHORD_LIMIT = 8
const DEFAULT_EDIT = true

function parseChordsFromQuery(route: Route): (ChordProgressionChord | undefined)[] {
  const query = route.query as ChordProgressionQuery
  return ChordProgressionChord.fromQuery(query.chords)
}

function saveChords(route: Route, router: VueRouter, chords: (ChordProgressionChord | undefined)[]): void {
  const query = Object.assign({}, route.query, {
    chords: ChordProgressionChord.toQuery(chords)
  })
  saveState(query, route.query, route.path, router)
}

export function parseEditFromQuery(route: Route): boolean {
  const query = route.query as ChordProgressionQuery
  return (query.edit === undefined || query.edit === null) ? DEFAULT_EDIT : query.edit === 'true' || query.edit === true
}

export function saveEdit(route: Route, router: VueRouter, edit: boolean): void {
  const query = Object.assign({}, route.query, {
    edit
  })
  saveState(query, route.query, route.path, router)
}

/**
 * Get the index to add the next chord to. If index is specified use that; if a default index is specified fallback to that; if neither is specified
 * add to the end of the progression
 * @param list The current list
 * @param index An explicit index to use
 */
function getIndex<T>(list: Array<T>, index?: number): number {
  const isSet = (n?: number) => !(n === null || n === undefined)
  
  if (isSet(index)) return index as number
  return list.length
}

export function getChordProgression(route: Route, scale: ScaleInKey): ChordProgression {
  return new ChordProgression(parseChordsFromQuery(route), scale)
}

export function setChord(route: Route, router: VueRouter, setChord: SetChordParameters): void {
  const chords = parseChordsFromQuery(route)
  const { index, degree, variantType, inversion, octave} = setChord
  const setIndex = getIndex(chords, index)
  
  if (setIndex >= CHORD_LIMIT) {
    console.error(`Can't have more than ${CHORD_LIMIT} chords in a progression`)
    return
  }
  
  const arrCopy = [...chords]
  arrCopy[setIndex] = new ChordProgressionChord(degree, variantType, inversion, octave)
  saveChords(route, router, arrCopy)
}

export function setChords(route: Route, router: VueRouter, setChords: (ChordProgressionChord | undefined)[]): void {
  saveChords(route, router, setChords)
}

export function insertChord(route: Route, router: VueRouter, setChord: SetChordParameters): void {
  if (setChord.index === undefined) {
    console.error('index needs to be set to insert chord')
    return
  }
  if (setChord.index >= CHORD_LIMIT) {
    console.error(`Can't have more than ${CHORD_LIMIT} chords in a progression`)
    return
  }

  const chords = parseChordsFromQuery(route)
  const { index, degree, variantType, inversion, octave} = setChord
  const newChord = new ChordProgressionChord(degree, variantType, inversion, octave)
  const newArray = [...chords.slice(0, index), newChord, ...chords.slice(index)]
  saveChords(route, router, newArray)
}

export function deleteChord(route: Route, router: VueRouter, index: number): void {
  const chords = parseChordsFromQuery(route)
  const arrCopy = [...chords]
  delete arrCopy[index]
  saveChords(route, router, arrCopy)
}

export function clear(route: Route, router: VueRouter): void {
  saveChords(route, router, [])
}