import { ChordProgression } from './models/chordProgression';
import { ScaleInKey } from '@/application/models/ScaleInKey'
import MidiWriter from 'midi-writer-js'

function addNotes(track: MidiWriter.Track, notes: number[] | string, duration: number, velocity = 70) {
  track.addEvent(
    new MidiWriter.NoteEvent({ pitch: notes, duration, velocity })
  )
}

export function writeMidi(progression: ChordProgression, scale: ScaleInKey, arpeggio: boolean) {
  const track = new MidiWriter.Track()
  track.addEvent(new MidiWriter.ProgramChangeEvent({instrument: 1}));
  track.setTempo(200)
  progression.chords.forEach(chord => {
    if (chord) {
      const keyScaleChord = chord.getChord(scale, false)
      const notes = keyScaleChord.midiNotes(chord.octave, chord.inversion)
      if (arpeggio) {
        // Make sure we have 4 notes to play per bar
        const notes4 = notes.length === 3 ? [...notes, notes[0]] : notes
        notes4.forEach(note => {
          addNotes(track, [note], 4)
        })
      } else {
        addNotes(track, notes, 1)
      }
    } else {
      addNotes(track, 'C4', 1)
    }
  })
  const writer = new MidiWriter.Writer(track)
  return writer.dataUri()
}