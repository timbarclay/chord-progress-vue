export class ChordFunction {
  name: string
  cssClass: string
  order: number
  constructor(name: string, cssClass: string, order: number) {
    this.name = name
    this.cssClass = cssClass
    this.order = order
  }
}

export const tonic = new ChordFunction('tonic', 'tonic', 0)
export const predominant = new ChordFunction('predominant', 'predominant', 1)
export const dominant = new ChordFunction('dominant', 'dominant', 2)

const chordFunctions: {[degree: number]: ChordFunction}  = {
  1: tonic,
  2: predominant,
  3: tonic,
  4: predominant,
  5: dominant,
  6: tonic,
  7: dominant
}

export function getChordFunction(degree: number) {
  return chordFunctions[degree] 
}