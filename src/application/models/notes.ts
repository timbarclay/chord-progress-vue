export class Note {
  asSharp: string
  asFlat: string
  constructor(asSharp: string, asFlat?: string) {
    this.asSharp = asSharp
    this.asFlat = asFlat || asSharp
  }

  enharmonic(isFlats: boolean) {
    return isFlats ? this.asFlat : this.asSharp
  }
}

export const notes = [
  new Note('C'),
  new Note('C#', 'Db'),
  new Note('D'),
  new Note('D#', 'Eb'),
  new Note('E'),
  new Note('F'),
  new Note('F#', 'Gb'),
  new Note('G'),
  new Note('G#', 'Ab'),
  new Note('A'),
  new Note('A#', 'Bb'),
  new Note('B')
]

/**
 * Get a note from a string. NB, this should only be used for scale roots - if you use it for other scale degrees, you can get into all sorts of hot water with complicated enharmonics
 * @param note 
 * @returns 
 */
export function fromString(note: string): Note | undefined {
  const noteUpper = note.toUpperCase()
  return notes.find(n => n.asSharp.toUpperCase() === noteUpper || n.asFlat.toUpperCase() === noteUpper) 
}
