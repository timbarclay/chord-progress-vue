export const octaves = [2, 3, 4]

export const minOctave = Math.min(...octaves)
export const maxOctave = Math.max(...octaves)
export const defaultOctave = octaves[Math.floor(octaves.length / 2)]