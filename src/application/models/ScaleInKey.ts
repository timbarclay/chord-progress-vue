import { Scale } from './scales'
import { Note } from './notes'
import { Chord, ChordVariants } from './chords'
import * as TonalScale from 'tonal-scale'
import * as TonalNote from 'tonal-note'
import { ChordFunction, getChordFunction } from './chordFunctions'
import _sortBy from 'lodash/sortBy'
import _zip from 'lodash/zip'

export class KeyScaleChord {
  scaleRoot: Note
  /** This is the note name we want to display, it may be different depending on whether the scale root is sharp or flat */
  note: string
  /** This is a canonical and consistent version of the above. Regardless of whether we're showing sharps or flats, this will be sharp for consistency */
  canonicalNote: string
  degree: number
  chord: Chord
  constructor (scaleRoot: Note, note: string, canonicalNote: string, degree: number, chord: Chord) {
    this.scaleRoot = scaleRoot
    this.note = note
    this.canonicalNote = canonicalNote
    this.degree = degree
    this.chord = chord
  }

  get chordSymbol() {
    return this.chord.asChordSymbol(this.note)
  }

  get chordName() {
    return this.chord.asChordName(this.note)
  }

  get chordNumeral() {
    return this.chord.asChordNumeral(this.degree)
  }

  // Only used in tests
  notes(startOctave: number, inversion: number) {
    return this.midiNotes(startOctave, inversion).map(n => TonalNote.fromMidi(n, true))
  }

  /**
   * Get the midi notes to sound out this chord.
   * The slightly fiddly part of this is working out which octave each note should appear in. Octaves run from C-C, so we need to work out which (if any) notes
   * need to appear in the next octave. E.g., in a major chord starting on G4, the notes need to be G4, B4, D5. The D needs to fall into the next 8ve
   * @param startOctave The octave that the root of the scale appears in
   * @param inversion 
   * @returns An array of midi note values
   */
  midiNotes(startOctave: number, inversion: number): number[] {
    const noteLetter = (note: string) => note.slice(0,1)

    const cMajor = TonalScale.notes('C major')
    const fromRoot = cMajor.slice(cMajor.indexOf(noteLetter(this.scaleRoot.asSharp)))
    const octave = fromRoot.includes(noteLetter(this.canonicalNote)) ? startOctave : startOctave + 1
    const rootNote = this.canonicalNote + octave
    const rootMidi = TonalNote.midi(rootNote) as number
    return this.chord.inversions[inversion].intervals.map(interval => rootMidi + (interval - 1))
  }
}

export class KeyScaleDegree {
  note: string
  degree: number
  chords: ChordVariants<KeyScaleChord>
  chordFunction: ChordFunction
  constructor (note: string, degree: number, chords: ChordVariants<KeyScaleChord>, chordFunction: ChordFunction) {
    this.note = note
    this.degree = degree
    this.chords = chords
    this.chordFunction = chordFunction
  }

  get degreeNumeral() {
    // We don't care whether this is triad or 7th here, both have the same major/minor quality
    return this.chords.triad.chord.asChordNumeral(this.degree)
  }
}

export class ScaleInKey {
  key: Note
  scale: Scale
  
  constructor (key: Note, scale: Scale) {
    this.key = key
    this.scale = scale
  }

  /**
   * Note that this array is zero indexed but the degree property will be the actual degree. So degrees[0] will be degree 1 of the scale
   */
  degrees (isFlats: boolean): KeyScaleDegree[] {
    const sharpNotes = this.scaleNotes(false)
    const enharmonicNotes = this.scaleNotes(isFlats)
    return _zip(enharmonicNotes, sharpNotes).map((notes: (string | undefined)[], i: number) => {
      const scaleDegree = this.scale.degrees[i]
      const enharmonicNote = notes[0] as string
      const canonicalNote = notes[1] as string
      const degreeNum = i + 1
      return new KeyScaleDegree(enharmonicNote, degreeNum, new ChordVariants(
        new KeyScaleChord(this.key, enharmonicNote, canonicalNote, degreeNum, scaleDegree.chords.triad),
        new KeyScaleChord(this.key, enharmonicNote, canonicalNote, degreeNum, scaleDegree.chords.seventh)),
        getChordFunction(degreeNum))
    })
  }

  /**
   * Get a scale degree by its degree number from 1-7. Note these start at 1, not 0.
   * @param degree degree number
   */
  getDegree(degree: number, isFlats: boolean) {
    return this.degrees(isFlats)[degree - 1]
  }

  degreesByFunction(isFlats: boolean): KeyScaleDegree[] {
    return _sortBy(this.degrees(isFlats), degree => degree.chordFunction.order)
  }

  scaleNotes(isFlats: boolean): string[] {
    return TonalScale.notes(`${this.key.enharmonic(isFlats)} ${this.scale.tonalName}`)
  }

  name(isFlats: boolean) {
    return `${this.key.enharmonic(isFlats)} ${this.scale.name}`
  }
}