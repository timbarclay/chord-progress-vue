import { ScaleInKey, KeyScaleChord, KeyScaleDegree } from "./ScaleInKey"
import { VariantType, getVariant } from './chords'
import { DegreeChordOption } from './chordWeightings'
import _dropRightWhile from 'lodash/dropRightWhile'

/**
 * Matches strings of the form: degree.variant.inversion.octave
 * Correctly matches inversions depending on variant, so triad can have 0-2; 7th can have 0-3
 * Each value is in its own group
 * If the chord is empty, it's represented by: r
 */
export const queryChordRegex = /^([1-7])\.(?:(?:(3)\.([0-2]))|(?:(7)\.([0-3])))\.([2-4])$|^r$/

export const queryChordSeparater = ';'

export class ChordProgressionChord {
  degree: number
  variantType: VariantType<KeyScaleChord>
  inversion: number
  octave: number
  constructor(degree: number, variantType: VariantType<KeyScaleChord>, inversion: number, octave: number) {
    this.degree = degree
    this.variantType = variantType
    this.inversion = inversion
    this.octave = octave
  }

  getChord(scale: ScaleInKey, isFlats: boolean): KeyScaleChord {
    return this.variantType.func(this.getDegree(scale, isFlats).chords)
  }

  getDegree(scale: ScaleInKey, isFlats: boolean): KeyScaleDegree {
    return scale.degrees(isFlats)[this.degree - 1]
  }

  get degreeChordOption(): DegreeChordOption {
    return {
      variantType: this.variantType.name,
      inversion: this.inversion,
      degree: this.degree
    }
  }

  getMidiNotes(scale: ScaleInKey): number[] {
    return this.getChord(scale, false).midiNotes(this.octave, this.inversion)
  }

  chordToQuery(): string {
    return `${this.degree}.${this.variantType.numberKey}.${this.inversion}.${this.octave}`
  }

  static toQuery(chords: ChordProgressionChordOrUndefined[]): string | null {
    const chordStrings =  _dropRightWhile(chords, c => !c).map(c => c ? c.chordToQuery() : 'r')
    return chordStrings.length > 0 ? chordStrings.join(queryChordSeparater) : null
  }

  static chordFromQuery(chord: string): ChordProgressionChord | undefined {
    if (!chord) return undefined
    const match = chord.match(queryChordRegex)
    if (!match) return undefined
    const matches = match.filter(m => !!m) // js returned undefined for non-capturing groups, so here we filter those out
    if (matches.length !== 5) return undefined // if it's an empty chord (or just garbled) it will return undefined here
    return new ChordProgressionChord(+matches[1], getVariant<KeyScaleChord>(matches[2]), +matches[3], +matches[4])
  }

  static fromQuery(chords?: string): ChordProgressionChordOrUndefined[] {
    if (!chords) return []
    return _dropRightWhile(chords.split(queryChordSeparater).map(ChordProgressionChord.chordFromQuery), c => !c)
  }
}

export type ChordProgressionChordOrUndefined = ChordProgressionChord | undefined

export class ChordProgression {
  chords: ChordProgressionChordOrUndefined[]
  scale: ScaleInKey

  constructor(chords: ChordProgressionChordOrUndefined[], scale: ScaleInKey) {
    this.chords = chords;
    this.scale = scale;
  }

  get isEmpty() {
    return this.chords.length === 0
  }
}

export interface SetChordParameters {
  degree: number
  variantType: VariantType<KeyScaleChord>
  inversion: number
  octave: number
  index?: number
}