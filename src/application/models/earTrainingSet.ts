import { ScaleInKey, KeyScaleChord } from './ScaleInKey'
import { ChordProgression, ChordProgressionChord } from './chordProgression'
import { getVariant, VariantType } from './chords'

export class EarTrainingSet {
  degrees: number[]
  constructor(degrees: number[]) {
    this.degrees = degrees
  }
}

function chooseRandom<T>(list: T[]): T {
  return list[Math.floor(Math.random() * list.length)]
}

function range(to: number): number[] {
  return [...Array(to).keys()]
}

export class EarTrainingRestrictions {
  variantTypes: string[]
  inversions: boolean
  octaves: number[]
  constructor(variantTypes: string[], inversions: boolean, octaves: number[]) {
    this.variantTypes = variantTypes
    this.inversions = inversions
    this.octaves = octaves
  }

  chooseVariant(): string {
    return chooseRandom(this.variantTypes)
  }

  chooseOctave(): number {
    return chooseRandom(this.octaves)
  }

  chooseInversion<T>(variantType: VariantType<T>) {
    return this.inversions ? chooseRandom(range(variantType.numberOfInversion)) : 0
  }
}

export class EarTrainingGenerator {
  trainingSet: EarTrainingSet
  restrictions: EarTrainingRestrictions
  scaleInKey: ScaleInKey
  constructor(trainingSet: EarTrainingSet, restrictions: EarTrainingRestrictions, scaleInKey: ScaleInKey) {
    this.trainingSet = trainingSet
    this.restrictions = restrictions
    this.scaleInKey = scaleInKey
  }

  generateProgression() {
    const chords = this.trainingSet.degrees.map(degree => {
      const chosenVariant = this.restrictions.chooseVariant()
      const variantType = getVariant<KeyScaleChord>(chosenVariant)
      const chosenInversion = this.restrictions.chooseInversion(variantType)
      const chosenOctave = this.restrictions.chooseOctave()
      return new ChordProgressionChord(degree, getVariant<KeyScaleChord>(chosenVariant), chosenInversion, chosenOctave)
    })
    const withBuffer = [...chords, undefined] // add an empty slot at the end so there's a break between progressions
    return new ChordProgression(withBuffer, this.scaleInKey)
  }
}