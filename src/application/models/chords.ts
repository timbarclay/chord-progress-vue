import { toRoman } from 'roman-numerals'

export class Inversion {
  number: number
  intervals: number[]
  constructor(number: number, intervals: number[]) {
    this.number = number;
    this.intervals = intervals
  }

  get name(): string {
    switch(this.number) {
      case 0:
        return 'root'
      case 1:
        return '1st'
      case 2:
        return '2nd'
      case 3:
        return '3rd'
      default:
        // we won't ever get here unless we start supporting more chord extensions
        return `${this.number}th`
    }
  }
}

export class Chord {
  name: string
  chordSymbolSuffix: string
  universalKeySuffix: string
  isMajor: boolean
  intervals: number[]
  
  constructor (name: string, chordSymbolSuffix: string, universalKeySuffix: string, isMajor: boolean, intervals: number[]) {
    this.name = name
    this.chordSymbolSuffix = chordSymbolSuffix
    this.universalKeySuffix = universalKeySuffix
    this.isMajor = isMajor
    this.intervals = intervals
  }

  asChordSymbol (note: string) {
    return `${note}${this.chordSymbolSuffix}`
  }

  asChordNumeral (degree: number) {
    const numeral = toRoman(degree)
    const cased = this.isMajor ? numeral.toUpperCase() : numeral.toLowerCase()
    return cased
    //return `${cased}${this.universalKeySuffix}`
  }

  asChordName (note: string) {
    return `${note} ${this.name}`
  }

  get inversions(): Inversion[] {
    return this.intervals.map((_, i) =>
      new Inversion(i, [...this.intervals.slice(i), ...this.intervals.slice(0, i).map(n => n + 12)]))
  }
}

export class ChordVariants<T> {
  triad: T
  seventh: T
  constructor (triad: T, seventh: T) {
    this.triad = triad
    this.seventh = seventh
  }
}

export type VariantFunc<T> = (variant: ChordVariants<T>) => T

export function triadFunc<T>(variant: ChordVariants<T>): T { return variant.triad }
export function seventhFunc<T>(variant: ChordVariants<T>): T { return variant.seventh }

export type VariantKey = 'triad' | '7th'
export const TRIAD: VariantKey = 'triad'
export const SEVENTH: VariantKey = '7th'

export class VariantType<T> {
  name: VariantKey
  func: VariantFunc<T>
  numberOfInversion: number
  numberKey: number
  constructor(name: VariantKey, func: VariantFunc<T>, numberOfInversion: number, numberKey: number) {
    this.name = name
    this.func = func
    this.numberOfInversion = numberOfInversion
    this.numberKey = numberKey
  }
}

export const variants = [
  new VariantType(TRIAD, triadFunc, 3, 3),
  new VariantType(SEVENTH, seventhFunc, 4, 7)
]

export function getVariant<T>(variant: string) {
  switch(variant) {
    case SEVENTH:
    case '7':
      return new VariantType<T>(SEVENTH, (variant: ChordVariants<T>) => variant.seventh, 4, 7)
    case TRIAD:
    case '3':
    default:
      return new VariantType<T>(TRIAD, (variant: ChordVariants<T>) => variant.triad, 3, 3)
  }
}

export const majorChord = new Chord('major', '', '', true, [1, 5, 8])
export const minorChord = new Chord('minor', 'm', '', false, [1, 4, 8])
export const diminishedChord = new Chord('diminished', '<sup>o</sup>', '<sup>o</sup>', false, [1, 4, 7])
export const augmentedChord = new Chord('augmented', '<sup>+</sup>', '<sup>+</sup>', true, [1, 5, 9])
export const major7th = new Chord('major 7th', '<sup>maj7</sup>', 'maj<sup>7</sup>', true, [1, 5, 8, 12])
export const minor7th = new Chord('minor 7th', 'm<sup>7</sup>', '<sup>7</sup>', false, [1, 4, 8, 11])
export const dominant7th = new Chord('dominant 7th', '<sup>7</sup>', '<sup>7</sup>', true, [1, 5, 8, 11])
export const minor7thFlat5 = new Chord('minor 7th flat 5', '<sup>ø</sup>', '<sup>ø</sup>', false, [1, 4, 7, 11])
export const minorMajor7 = new Chord('minor major 7', 'm<sup>M7</sup>', 'maj<sup>7</sup>', false, [1, 4, 8, 12])
export const major7thSharp5 = new Chord('major 7th sharp 5', '+<sup>M7</sup>', '+<sup>M7</sup>', true, [1, 5, 9, 12])
export const diminished7th = new Chord('diminished 7th', '<sup>o7</sup>', '<sup>o7</sup>', false, [1, 4, 7, 10])
