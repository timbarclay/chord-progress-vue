import { TRIAD, SEVENTH } from './chords';
import _ from 'lodash';

export interface ChordOption {
  variantType: string
  inversion: number
}

export interface DegreeChordOption {
  variantType: string
  inversion: number
  degree: number
}

export interface ChordWeightOverride {
  chordOpt: ChordOption
  override: number
}

export interface PreciseChordWeighting {
  weighting: number
  fromDegree?: number
  toDegree: number
  chord: ChordOption
}

const preciseChords: ChordOption[] = [
  {variantType: TRIAD, inversion: 0},
  {variantType: TRIAD, inversion: 1},
  {variantType: TRIAD, inversion: 2},
  {variantType: SEVENTH, inversion: 0},
  {variantType: SEVENTH, inversion: 1},
  {variantType: SEVENTH, inversion: 2},
  {variantType: SEVENTH, inversion: 3}
]

export const seventhWeightingFactor = 0.9

export class ChordChangeWeighting {
  defaultWeighting: number
  overrides: ChordWeightOverride[]
  constructor(defaultPreferences: number, overrides: ChordWeightOverride[] = []) {
    this.defaultWeighting = defaultPreferences;
    this.overrides = overrides
  }

  private get default7thPref() {
    return this.defaultWeighting * seventhWeightingFactor
  }

  preciseChordWeighting(chord: ChordOption, toDegree: number, fromDegree: number): PreciseChordWeighting {
    const override = _.find(this.overrides, o => _.isMatch(o.chordOpt, chord));

    return {
      fromDegree,
      toDegree,
      chord,
      weighting: override ? override.override
        : chord.variantType === SEVENTH ? this.default7thPref
          : this.defaultWeighting
    }
  }
}

export class ChordWeightings {
  chordPreferences: ChordChangeWeighting[]
  constructor(chordPreferences: ChordChangeWeighting[]) {
    this.chordPreferences = chordPreferences
  }

  getChords(degree: number, reverse: boolean): PreciseChordWeighting[] {
    return _.flatMap(this.chordPreferences, (chordPref, i) =>
      _.map(preciseChords, chord => {
        const toDegree = !reverse ? i + 1 : degree
        const fromDegree = !reverse ? degree : i + 1
        return chordPref.preciseChordWeighting(chord, toDegree, fromDegree)
      })
    )
  }
}

export class ScaleDegreeWeightings {
  degreeWeightings: ChordWeightings[]
  reversedWeightings: ChordWeightings[]
  constructor(degreePreferences: ChordChangeWeighting[][]) {
    const chordWeightings = degreePreferences.map(p => new ChordWeightings(p))
    
    this.degreeWeightings = chordWeightings
    this.reversedWeightings = this._reverseWeightings(chordWeightings)
  }

  private _reverseWeightings(weightings: ChordWeightings[]) {
    return weightings.map((weighting, i) => {
      return new ChordWeightings(weighting.chordPreferences.map((pref, j) => {
        return weightings[j].chordPreferences[i]
      }))
    })
  }

  /**
   * Get a 2 dimensional array that represents a matrix of weightings from every scale degree to every
   * other chord, so for example at index 0 is an array of weightings for every chord from degree 1
   */
  get allChordWeightings(): PreciseChordWeighting[][] {
    return this.degreeWeightings.map((p, i) => p.getChords(i + 1, false))
  }

  /**
   * Get a 2 dimensional array that represents a matrix of weightings to every chord from every scale
   * degree, so for example at index 0 is an array of weightings for every chord from degree 1
   */
  get reversedAllChordWeightings(): PreciseChordWeighting[][] {
    return this.reversedWeightings.map((p, i) => p.getChords(i + 1, true))
  }

  /**
   * Get the weighting of all chords from the given scale degree
   * @param fromDegree {number} The degree number to move from
   */
  chordsToMoveTo(fromDegree: number): PreciseChordWeighting[] {
    return this.allChordWeightings[fromDegree - 1]
  }

  /**
   * Get all the weightings from e
   * @param toDegree {number} The degree to move from
   */
  chordsToArriveFrom(toDegree: number): PreciseChordWeighting[] {
    //return this.reversedAllChordWeightings[toDegree - 1]
    return _.flatMap(this.reversedAllChordWeightings, a => a.filter(b => b.toDegree === toDegree))
  }
}
