import {
  Chord,
  ChordVariants,
  majorChord,
  minorChord,
  diminishedChord,
  augmentedChord,
  major7th,
  minor7th,
  dominant7th,
  minor7thFlat5,
  minorMajor7,
  major7thSharp5,
  diminished7th
} from './chords'

export class ScaleDegree {
  degree: number
  intervalFromRoot: number
  chords: ChordVariants<Chord>
  constructor (degree: number, intervalFromRoot: number, chords: ChordVariants<Chord>) {
    this.degree = degree
    this.intervalFromRoot = intervalFromRoot
    this.chords = chords
  }
}

export class Scale {
  name: string
  tonalName: string
  isMajor: boolean
  degrees: ScaleDegree[]
  
  constructor (name: string, tonalName: string, isMajor: boolean, degrees: ScaleDegree[]) {
    this.name = name
    this.tonalName = tonalName
    this.isMajor = isMajor
    this.degrees = degrees
  }
}

export const majorScale = new Scale('major', 'major', true, [
  new ScaleDegree(1, 1, new ChordVariants(majorChord, major7th)),
  new ScaleDegree(2, 3, new ChordVariants(minorChord, minor7th)),
  new ScaleDegree(3, 5, new ChordVariants(minorChord, minor7th)),
  new ScaleDegree(4, 6, new ChordVariants(majorChord, major7th)),
  new ScaleDegree(5, 8, new ChordVariants(majorChord, dominant7th)),
  new ScaleDegree(6, 10, new ChordVariants(minorChord, minor7th)),
  new ScaleDegree(7, 12, new ChordVariants(diminishedChord, minor7thFlat5))
])

export const naturalMinorScale = new Scale('natural minor', 'aeolian', false, [
  new ScaleDegree(1, 1, new ChordVariants(minorChord, minor7th)),
  new ScaleDegree(2, 3, new ChordVariants(diminishedChord, minor7thFlat5)),
  new ScaleDegree(3, 4, new ChordVariants(majorChord, major7th)),
  new ScaleDegree(4, 6, new ChordVariants(minorChord, minor7th)),
  new ScaleDegree(5, 8, new ChordVariants(minorChord, minor7th)),
  new ScaleDegree(6, 9, new ChordVariants(majorChord, major7th)),
  new ScaleDegree(7, 11, new ChordVariants(majorChord, dominant7th))
])

export const harmonicMinorScale = new Scale('harmonic minor', 'harmonic minor', false, [
  new ScaleDegree(1, 1, new ChordVariants(minorChord, minorMajor7)),
  new ScaleDegree(2, 3, new ChordVariants(diminishedChord, minor7thFlat5)),
  new ScaleDegree(3, 4, new ChordVariants(augmentedChord, major7thSharp5)),
  new ScaleDegree(4, 6, new ChordVariants(minorChord, minor7th)),
  new ScaleDegree(5, 8, new ChordVariants(majorChord, dominant7th)),
  new ScaleDegree(6, 9, new ChordVariants(majorChord, major7th)),
  new ScaleDegree(7, 12, new ChordVariants(diminishedChord, diminished7th))
])

const allScales = [majorScale, naturalMinorScale, harmonicMinorScale]
export function getScale(name: string): Scale {
  return allScales.find(s => s.name === name) || majorScale
}