export class SelectableT<T> {
  item: T
  selected: boolean

  constructor (item: T, selected: boolean) {
    this.item = item
    this.selected = selected
  }
}

export function selectRandom<T>(list: T[]): T {
  return list[Math.floor(Math.random() * list.length)]
}