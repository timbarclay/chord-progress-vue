import VueRouter, { Route } from 'vue-router'
import { notes, Note, fromString } from "@/application/models/notes";
import { getScale, Scale } from './models/scales';
import { saveState } from './managerHelpers';

/**
 * We persist the key in the query string for pages that are based on a single key at a time.
 * Note the persisted key always uses the sharp name for black notes regardless of other settings, e.g. always C#, never Db
 */
interface KeyQuery {
  note?: string
  scale?: string
}

const DEFAULT_NOTE = notes[0]
const DEFAULT_SCALE = 'major'

export function getKeyNote(route: Route): Note {
  const { note } = route.query as KeyQuery
  if (!note) return DEFAULT_NOTE
  const noteObj = fromString(note)
  return noteObj || DEFAULT_NOTE
}

export function getKeyScale(route: Route): Scale {
  const { scale } = route.query as KeyQuery
  return getScale((scale || DEFAULT_SCALE).toLowerCase())
}

export function setSelectedNote(route: Route, router: VueRouter, note: Note): void {
  const query = Object.assign({}, route.query, {
    note: note.asSharp
  })
  saveState(query, route.query, route.path, router)
}

export function setSelectedScale(route: Route, router: VueRouter, scale?: Scale): void {
  const scaleName = scale?.name || DEFAULT_SCALE
  const query = Object.assign({}, route.query, {
    scale: scaleName
  })
  saveState(query, route.query, route.path, router)
}