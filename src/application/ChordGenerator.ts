import { ChordProgressionChord, ChordProgressionChordOrUndefined } from './models/chordProgression';
import { chordWeightings, firstChordWeightings } from './chordWeightings'
import { allWeightingsToMoveFrom, allWeightingsToMoveTo, mergeWeightings } from './ChordSuggester'
import { getVariant } from './models/chords'
import { PreciseChordWeighting } from './models/chordWeightings'
import { KeyScaleChord, ScaleInKey } from './models/ScaleInKey'
import { majorScale } from './models/scales'
import { notes } from './models/notes'
import { octaves, defaultOctave } from './models/octaves'
import _minBy from 'lodash/minBy'
import _takeWhile from 'lodash/takeWhile'

function makeRejectionSamplingTable(weightings: PreciseChordWeighting[]) {
  return weightings.flatMap(weight => {
    const weightNum = Math.floor(weight.weighting * 100)
    return Array.from({length: weightNum}, () => weight)
  })
}

function getNoteDistanceScore(aNotes: number[], bNotes: number[], cNotes?: number[]): number {
  const diff = (a: number, b: number) => Math.abs(a - b)
  
  return aNotes.map((note, i) => {
    const otherB = bNotes[i] || 0
    const otherC = cNotes && cNotes[i] || 0
    return diff(note, otherB) + diff(note, otherC)
  }).reduce((prev, curr) => prev + curr)
}

function defaultKey() {
  return new ScaleInKey(notes[0], majorScale)
}

function chooseOctaveForChord(lastChord: ChordProgressionChord, chosenChord: PreciseChordWeighting, defaultOctave: number, followingChord?: ChordProgressionChord) {
  if (!lastChord) {
    return defaultOctave
  }
  
  const scale = defaultKey()
  const lastChordNotes = lastChord.getMidiNotes(scale)
  const nextChordNotes = followingChord ? followingChord.getMidiNotes(scale) : undefined
  
  const chosenChordDegree = scale.degrees(false)[chosenChord.toDegree - 1]
  const { variantType, inversion } = chosenChord.chord
  const scaleChord = getVariant(variantType).func(chosenChordDegree.chords) as KeyScaleChord

  const octaveScores = octaves.map(octave => {
    return {
      octave,
      score: getNoteDistanceScore(lastChordNotes, scaleChord.midiNotes(octave, inversion), nextChordNotes)
    }
  })
  
  const closest = _minBy(octaveScores, o => o.score)
  return closest ? closest.octave : defaultOctave
}

/**
 * Generate a new chord based on the weightings from the last chord, and optionally also taking into account the weightings to the following chord if there is one
 * @param lastChord The chord that this chord should follow
 * @param followingChord The chord that will follow this chord (optional)
 * @param weightings A chord weightings map
 * @param startWeightings A set of weightings for if there is no lastChord to follow
 */
function generateChord(lastChord: ChordProgressionChord, followingChord?: ChordProgressionChord, weightings = chordWeightings, startWeightings = firstChordWeightings): ChordProgressionChord {
  function relevantWeightings() {
    const weightingsFromLastChord = lastChord ? allWeightingsToMoveTo(lastChord, weightings) : startWeightings
    return followingChord ? mergeWeightings(weightingsFromLastChord, allWeightingsToMoveFrom(followingChord, weightings)) : weightingsFromLastChord
  }
  
  const rejectionSamplingTable = makeRejectionSamplingTable(relevantWeightings())
  const sampleNum = Math.floor(Math.random() * (rejectionSamplingTable.length - 1))
  const chosenChord = rejectionSamplingTable[sampleNum]
  const octave = chooseOctaveForChord(lastChord, chosenChord, defaultOctave, followingChord)
  return new ChordProgressionChord(chosenChord.toDegree, getVariant(chosenChord.chord.variantType), chosenChord.chord.inversion, octave)
}

/**
 * Returns a list of chords containing the passed in list with the specified number of generated chords added to the end.
 * The passed in chord progression can be empty, but if it is populated then the last chord will be used as the jumping off point
 * for the first generated chord
 * @param chordProgression An existing set of chords
 * @param numberToGenerate The number of new chords to generate
 */
export function generateChords(chordProgression: ChordProgressionChordOrUndefined[], numberToGenerate: number): ChordProgressionChordOrUndefined[] {
  const indexToStart = chordProgression.length
  const revisedNumber = Math.min(chordProgression.length + numberToGenerate, 8) - chordProgression.length
  const generated: ChordProgressionChord[] = []
  
  for (let i = 0; i < revisedNumber; i++) {
    const indexInProgression = i + indexToStart
    const lastChord = i === 0 ? chordProgression[chordProgression.length - 1] : generated[i - 1]
    const followChord = (indexInProgression + 1) % 4 === 0 ? chordProgression[0] : undefined
    generated[i] = generateChord(lastChord as ChordProgressionChord, followChord)
  }

  return chordProgression.concat(generated)
}

function chooseParsimoniousChord(lastChord: ChordProgressionChord, nextChord: ChordProgressionChord, scale: ScaleInKey): ChordProgressionChord {
  const lastChordMidi = lastChord.getMidiNotes(scale)
    
  const thisChord = nextChord.getChord(scale, false)
  const inversions = [...Array(nextChord.variantType.numberOfInversion).keys()]
  const scores = octaves.flatMap(octave => {
    return inversions.map(inversion => {
      const thisChordMidi = thisChord.midiNotes(octave, inversion)
      return {
        octave,
        inversion,
        score: getNoteDistanceScore(lastChordMidi, thisChordMidi)
      }
    })
  })
  const closest = _minBy(scores, s => s.score)
  return closest
    ? new ChordProgressionChord(nextChord.degree, nextChord.variantType, closest.inversion, closest.octave)
    : nextChord // I can't imagine why this should ever happen but to keep the typings happy we need to account for closest being undefined
}

export function parsimonifyChords(chordProgression: ChordProgressionChordOrUndefined[], scale: ScaleInKey): ChordProgressionChordOrUndefined[] {
  if (chordProgression.filter(c => !!c).length === 0) {
    return chordProgression
  }
  
  // we want to start with the first non-empty chord, so here we'll collect any empty chords at the start to re-attach later
  const emptyStart = _takeWhile(chordProgression, c => !c)
  const restOfChords = chordProgression.slice(emptyStart.length)

  const newChords: ChordProgressionChordOrUndefined[] = []
  restOfChords.forEach((chord, i) => {
    if (!chord || i === 0 || !newChords[i - 1]) {
      // If this isn't a chord, or there isn't one immediately before it, return as is
      newChords.push(chord)
      return
    }

    const lastChord = newChords[i - 1] as ChordProgressionChord
    newChords.push(chooseParsimoniousChord(lastChord, chord, scale))
  })

  return emptyStart.concat(newChords)
}