import { Dictionary, VueRouter } from 'vue-router/types/router';
import _isEqual from 'lodash/isEqual'

type Query = Dictionary<string | (string | null)[] | null | undefined>

export function saveState(newQuery: Query, oldQuery: Query, path: string, router: VueRouter) {
  if (!_isEqual(oldQuery, newQuery)) {
    router.replace({
      path,
      query: newQuery
    })
  }
}