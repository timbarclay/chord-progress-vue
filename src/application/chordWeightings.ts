import { PreciseChordWeighting, ChordWeightOverride, ScaleDegreeWeightings, ChordChangeWeighting, ChordOption } from './models/chordWeightings'
import { TRIAD, SEVENTH } from './models/chords'

// This is just a convenience for brevity
function c(defaultPreferences: number, overrides: ChordWeightOverride[] = []){
  return new ChordChangeWeighting(defaultPreferences, overrides)
}

const self = c(0.3)

const triad1 = {
  variantType: TRIAD,
  inversion: 1
} as ChordOption

/**
 * This is one specific set of weightings. In future this could be configurable to favour
 * e.g. classical progressions vs rock progressions vs Bon Jovi's favourite progressions etc
 */
const tymoczkoWeightings = new ScaleDegreeWeightings([
  [self, c(0.8), c(0.2), c(0.9), c(0.8), c(0.8), c(0.7)],
  [c(0.1), self, c(0.1), c(0.1), c(0.9), c(0.1), c(0.9)],
  [c(0.8, [{chordOpt: triad1, override: 0.9}]), c(0.8), self, c(0.9), c(0.9), c(0.5), c(0.8)],
  [c(0.9), c(0.8), c(0.1), self, c(0.9), c(0.1), c(0.8)],
  [c(0.9), c(0.1), c(0.1), c(0.7), self, c(0.7), c(0.1)],
  [c(0.8, [{chordOpt: triad1, override: 0.7}]), c(0.8), c(0.1), c(0.8), c(0.8), self, c(0.8)],
  [c(0.9), c(0.1), c(0.1), c(0.1), c(0.9), c(0.1), self]
])

/**
 * Based on https://shedthemusic.com/diatonic-chord-progressions-2
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const shedMusicWeightings = new ScaleDegreeWeightings([
  [self, c(0.8), c(0.8), c(0.8), c(0.8), c(0.8), c(0.8)],
  [c(0.2), self, c(0.8), c(0.9), c(0.9), c(0.2), c(0.2)],
  [c(0.9), c(0.8), self, c(0.8), c(0.2), c(0.9), c(0.2)],
  [c(0.9), c(0.2), c(0.8), self, c(0.9), c(0.8), c(0.2)],
  [c(0.9), c(0.2), c(0.2), c(0.9), self, c(0.8), c(0.2)],
  [c(0.8), c(0.8), c(0.2), c(0.9), c(0.9), self, c(0.2)],
  [c(0.9), c(0.1), c(0.1), c(0.1), c(0.9), c(0.1), self]
])

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const madeUpFirstChordWeightings: PreciseChordWeighting[] = [
  { weighting: 0.9, toDegree: 1, chord: { variantType: TRIAD, inversion: 0 } },
  { weighting: 0.9, toDegree: 1, chord: { variantType: TRIAD, inversion: 1 } },
  { weighting: 0.9, toDegree: 1, chord: { variantType: TRIAD, inversion: 2 } },
  { weighting: 0.8, toDegree: 1, chord: { variantType: SEVENTH, inversion: 0 } },
  { weighting: 0.7, toDegree: 3, chord: { variantType: TRIAD, inversion: 0 } },
  { weighting: 0.7, toDegree: 3, chord: { variantType: TRIAD, inversion: 1 } },
  { weighting: 0.7, toDegree: 6, chord: { variantType: TRIAD, inversion: 0 } },
  { weighting: 0.7, toDegree: 6, chord: { variantType: TRIAD, inversion: 1 } },
]

const shedMusicFirstChordWeightings: PreciseChordWeighting[] = [
  { weighting: 0.9, toDegree: 1, chord: { variantType: TRIAD, inversion: 0 } },
  { weighting: 0.9, toDegree: 1, chord: { variantType: TRIAD, inversion: 1 } },
  { weighting: 0.9, toDegree: 1, chord: { variantType: TRIAD, inversion: 2 } },
  { weighting: 0.8, toDegree: 1, chord: { variantType: SEVENTH, inversion: 0 } },
  { weighting: 0.8, toDegree: 4, chord: { variantType: TRIAD, inversion: 0 } },
  { weighting: 0.8, toDegree: 4, chord: { variantType: TRIAD, inversion: 1 } },
  { weighting: 0.8, toDegree: 6, chord: { variantType: TRIAD, inversion: 0 } },
  { weighting: 0.8, toDegree: 6, chord: { variantType: TRIAD, inversion: 1 } }
]

export const chordWeightings = tymoczkoWeightings
export const firstChordWeightings = shedMusicFirstChordWeightings