import { ChordProgressionChord, ChordProgression } from '@/application/models/chordProgression'
import { PreciseChordWeighting, ScaleDegreeWeightings } from './models/chordWeightings'
import { chordWeightings, firstChordWeightings } from './chordWeightings'
import _orderBy from 'lodash/orderBy'

export function allWeightingsToMoveTo(from: ChordProgressionChord | undefined, weightings: ScaleDegreeWeightings) {
  if (!from) return []
  return weightings.chordsToMoveTo(from.degree)
}

export function allWeightingsToMoveFrom(to: ChordProgressionChord | undefined, weightings: ScaleDegreeWeightings) {
  if (!to) return []
  return weightings.chordsToArriveFrom(to.degree)
}

export function mergeWeightings(a: PreciseChordWeighting[], b: PreciseChordWeighting[]) {
  const mergeWeights = (aWeight: number, bWeight: number) => {
    if (aWeight && bWeight) return aWeight * bWeight
    return aWeight || bWeight
  }
  
  return a.map((weight, i) => {
    const aWeight = weight.weighting
    const bWeight = b[i].weighting
    return Object.assign({}, weight, { weighting: mergeWeights(aWeight, bWeight) })
  })
}

/**
 * Return a set of weightings to use as the basis for suggestions for the next chord to choose in a progression. This does slightly different things depending on which of
 * three states the current chord progression is in:
 *  1. empty - it returns a hard coded set of weightings for a starting chord
 *  2. end of phrase - if the chord we're suggesting would be the 4th or 8th chord in a progression we want to make sure it can loop well with the 1st chord in the progression
 *     so it will return weights derived both from the movement from the last chord as well as the movement to the first chord
 *  3. anything else - in other cases the weightings will be derived from the last chord in the progression
 * @param chordProgression The current chord progression
 * @param weightings A set of weightings to use
 * @param startWeightings A set of weights to use for the first chord in the progression
 * @param indexToSuggest The index of the chord in the progression that we want a suggestion for
 */
function weightingsForNextChord(chordProgression: ChordProgression, weightings: ScaleDegreeWeightings, startWeightings: PreciseChordWeighting[], indexToSuggest?: number): PreciseChordWeighting[] {
  const indexForNextChord = indexToSuggest || chordProgression.chords.length

  if (chordProgression.isEmpty || indexToSuggest === 0) {
    return startWeightings
  }

  if (indexForNextChord >= 8) {
    return []
  }

  const lastChord = chordProgression.chords[indexForNextChord - 1]
  if (!lastChord) {
    return []
  }
  
  if ((indexForNextChord + 1) % 4 === 0) { // TODO make progression length configurable maybe?
    const firstChord = chordProgression.chords[0]
    const to = allWeightingsToMoveTo(lastChord, weightings)
    const from = allWeightingsToMoveFrom(firstChord, weightings)
    return mergeWeightings(to, from)
  }
  
  return allWeightingsToMoveTo(lastChord, weightings)
}

const weightingThreshold = 0.7
const limit = 15

export function suggestNextChord(chordProgression: ChordProgression, index?: number, weightings = chordWeightings, startWeightings = firstChordWeightings): PreciseChordWeighting[] {
  const weights = weightingsForNextChord(chordProgression, weightings, startWeightings, index).filter(w => w.weighting >= weightingThreshold)
  const sorted = _orderBy(weights, ['weighting'], ['desc'])
  return sorted.slice(0, limit)
}
