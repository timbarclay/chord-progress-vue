import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import "./assets/app.scss";
import VueGtag from "vue-gtag";
import Buefy from 'buefy'
import VueHotkey from 'v-hotkey'
import VueScreen from 'vue-screen'

Vue.config.productionTip = false;

Vue.use(VueGtag, {
  config: { id: process.env.VUE_APP_GTAG_ID }
}, router);
Vue.use(Buefy)
Vue.use(VueHotkey, {
  'deg1': 49,
  'deg2': 50,
  'deg3': 51,
  'deg4': 52,
  'deg5': 53,
  'deg6': 54,
  'deg7': 55
})
Vue.use(VueScreen, 'bulma')

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
