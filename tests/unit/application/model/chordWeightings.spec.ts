import { ScaleDegreeWeightings, ChordChangeWeighting, ChordWeightOverride, seventhWeightingFactor } from "@/application/models/chordWeightings";
import { TRIAD, SEVENTH } from '@/application/models/chords';

// This is just a convenience for brevity
function c(defaultPreferences: number, overrides: ChordWeightOverride[] = []){
  return new ChordChangeWeighting(defaultPreferences, overrides)
}

const weightings = new ScaleDegreeWeightings([
  [c(0.5), c(0.9)],
  [c(0.2), c(0.4)]
])

describe('Scale degree weightings', () => {
  it('returns the weightings moving from degree 1 to all other chords', () => {
    const expected = [
      { fromDegree: 1, toDegree: 1, chord: { variantType: TRIAD, inversion: 0 }, weighting: 0.5 },
      { fromDegree: 1, toDegree: 1, chord: { variantType: TRIAD, inversion: 1 }, weighting: 0.5 },
      { fromDegree: 1, toDegree: 1, chord: { variantType: TRIAD, inversion: 2 }, weighting: 0.5 },
      { fromDegree: 1, toDegree: 1, chord: { variantType: SEVENTH, inversion: 0 }, weighting: 0.5 * seventhWeightingFactor },
      { fromDegree: 1, toDegree: 1, chord: { variantType: SEVENTH, inversion: 1 }, weighting: 0.5 * seventhWeightingFactor },
      { fromDegree: 1, toDegree: 1, chord: { variantType: SEVENTH, inversion: 2 }, weighting: 0.5 * seventhWeightingFactor },
      { fromDegree: 1, toDegree: 1, chord: { variantType: SEVENTH, inversion: 3 }, weighting: 0.5 * seventhWeightingFactor },
      { fromDegree: 1, toDegree: 2, chord: { variantType: TRIAD, inversion: 0 }, weighting: 0.9 },
      { fromDegree: 1, toDegree: 2, chord: { variantType: TRIAD, inversion: 1 }, weighting: 0.9 },
      { fromDegree: 1, toDegree: 2, chord: { variantType: TRIAD, inversion: 2 }, weighting: 0.9 },
      { fromDegree: 1, toDegree: 2, chord: { variantType: SEVENTH, inversion: 0 }, weighting: 0.9 * seventhWeightingFactor },
      { fromDegree: 1, toDegree: 2, chord: { variantType: SEVENTH, inversion: 1 }, weighting: 0.9 * seventhWeightingFactor },
      { fromDegree: 1, toDegree: 2, chord: { variantType: SEVENTH, inversion: 2 }, weighting: 0.9 * seventhWeightingFactor },
      { fromDegree: 1, toDegree: 2, chord: { variantType: SEVENTH, inversion: 3 }, weighting: 0.9 * seventhWeightingFactor }
    ]
    expect(weightings.chordsToMoveTo(1)).toEqual(expected)
  })

  it('returns the weightings moving from degree 2 to all other chords', () => {
    const expected = [
      { fromDegree: 2, toDegree: 1, chord: { variantType: TRIAD, inversion: 0 }, weighting: 0.2 },
      { fromDegree: 2, toDegree: 1, chord: { variantType: TRIAD, inversion: 1 }, weighting: 0.2 },
      { fromDegree: 2, toDegree: 1, chord: { variantType: TRIAD, inversion: 2 }, weighting: 0.2 },
      { fromDegree: 2, toDegree: 1, chord: { variantType: SEVENTH, inversion: 0 }, weighting: 0.2 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 1, chord: { variantType: SEVENTH, inversion: 1 }, weighting: 0.2 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 1, chord: { variantType: SEVENTH, inversion: 2 }, weighting: 0.2 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 1, chord: { variantType: SEVENTH, inversion: 3 }, weighting: 0.2 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 2, chord: { variantType: TRIAD, inversion: 0 }, weighting: 0.4 },
      { fromDegree: 2, toDegree: 2, chord: { variantType: TRIAD, inversion: 1 }, weighting: 0.4 },
      { fromDegree: 2, toDegree: 2, chord: { variantType: TRIAD, inversion: 2 }, weighting: 0.4 },
      { fromDegree: 2, toDegree: 2, chord: { variantType: SEVENTH, inversion: 0 }, weighting: 0.4 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 2, chord: { variantType: SEVENTH, inversion: 1 }, weighting: 0.4 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 2, chord: { variantType: SEVENTH, inversion: 2 }, weighting: 0.4 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 2, chord: { variantType: SEVENTH, inversion: 3 }, weighting: 0.4 * seventhWeightingFactor }
    ]
    expect(weightings.chordsToMoveTo(2)).toEqual(expected)
  })

  it('returns the weightings moving to degree 1 from all other chords', () => {
    const expected = [
      { fromDegree: 1, toDegree: 1, chord: { variantType: TRIAD, inversion: 0 }, weighting: 0.5 },
      { fromDegree: 1, toDegree: 1, chord: { variantType: TRIAD, inversion: 1 }, weighting: 0.5 },
      { fromDegree: 1, toDegree: 1, chord: { variantType: TRIAD, inversion: 2 }, weighting: 0.5 },
      { fromDegree: 1, toDegree: 1, chord: { variantType: SEVENTH, inversion: 0 }, weighting: 0.5 * seventhWeightingFactor },
      { fromDegree: 1, toDegree: 1, chord: { variantType: SEVENTH, inversion: 1 }, weighting: 0.5 * seventhWeightingFactor },
      { fromDegree: 1, toDegree: 1, chord: { variantType: SEVENTH, inversion: 2 }, weighting: 0.5 * seventhWeightingFactor },
      { fromDegree: 1, toDegree: 1, chord: { variantType: SEVENTH, inversion: 3 }, weighting: 0.5 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 1, chord: { variantType: TRIAD, inversion: 0 }, weighting: 0.2 },
      { fromDegree: 2, toDegree: 1, chord: { variantType: TRIAD, inversion: 1 }, weighting: 0.2 },
      { fromDegree: 2, toDegree: 1, chord: { variantType: TRIAD, inversion: 2 }, weighting: 0.2 },
      { fromDegree: 2, toDegree: 1, chord: { variantType: SEVENTH, inversion: 0 }, weighting: 0.2 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 1, chord: { variantType: SEVENTH, inversion: 1 }, weighting: 0.2 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 1, chord: { variantType: SEVENTH, inversion: 2 }, weighting: 0.2 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 1, chord: { variantType: SEVENTH, inversion: 3 }, weighting: 0.2 * seventhWeightingFactor }
    ]
    expect(weightings.chordsToArriveFrom(1)).toEqual(expected)
  })

  it('returns the weightings moving to degree 2 from all other chords', () => {
    const expected = [
      { fromDegree: 1, toDegree: 2, chord: { variantType: TRIAD, inversion: 0 }, weighting: 0.9 },
      { fromDegree: 1, toDegree: 2, chord: { variantType: TRIAD, inversion: 1 }, weighting: 0.9 },
      { fromDegree: 1, toDegree: 2, chord: { variantType: TRIAD, inversion: 2 }, weighting: 0.9 },
      { fromDegree: 1, toDegree: 2, chord: { variantType: SEVENTH, inversion: 0 }, weighting: 0.9 * seventhWeightingFactor },
      { fromDegree: 1, toDegree: 2, chord: { variantType: SEVENTH, inversion: 1 }, weighting: 0.9 * seventhWeightingFactor },
      { fromDegree: 1, toDegree: 2, chord: { variantType: SEVENTH, inversion: 2 }, weighting: 0.9 * seventhWeightingFactor },
      { fromDegree: 1, toDegree: 2, chord: { variantType: SEVENTH, inversion: 3 }, weighting: 0.9 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 2, chord: { variantType: TRIAD, inversion: 0 }, weighting: 0.4 },
      { fromDegree: 2, toDegree: 2, chord: { variantType: TRIAD, inversion: 1 }, weighting: 0.4 },
      { fromDegree: 2, toDegree: 2, chord: { variantType: TRIAD, inversion: 2 }, weighting: 0.4 },
      { fromDegree: 2, toDegree: 2, chord: { variantType: SEVENTH, inversion: 0 }, weighting: 0.4 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 2, chord: { variantType: SEVENTH, inversion: 1 }, weighting: 0.4 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 2, chord: { variantType: SEVENTH, inversion: 2 }, weighting: 0.4 * seventhWeightingFactor },
      { fromDegree: 2, toDegree: 2, chord: { variantType: SEVENTH, inversion: 3 }, weighting: 0.4 * seventhWeightingFactor }
    ]
    expect(weightings.chordsToArriveFrom(2)).toEqual(expected)
  })
})