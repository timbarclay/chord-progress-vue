import { fromString, Note } from '@/application/models/notes'
import { ScaleInKey } from '@/application/models/ScaleInKey'
import { majorScale, naturalMinorScale, harmonicMinorScale } from '@/application/models/scales'

const cMajor = new ScaleInKey(fromString('C') as Note, majorScale)
const fMajor = new ScaleInKey(fromString('F') as Note, majorScale)
const cSharpMinor = new ScaleInKey(fromString('C#') as Note, naturalMinorScale)
const aSharpHarmMinor = new ScaleInKey(fromString('A#') as Note, harmonicMinorScale)

describe('Scale in key', () => {
  it('shows scale notes', () => {
    expect(cMajor.scaleNotes(false)).toEqual(['C', 'D', 'E', 'F', 'G', 'A', 'B'])
    expect(fMajor.scaleNotes(false)).toEqual(['F', 'G', 'A', 'Bb', 'C', 'D', 'E'])
    expect(cSharpMinor.scaleNotes(false)).toEqual(['C#', 'D#', 'E', 'F#', 'G#', 'A', 'B'])
    expect(aSharpHarmMinor.scaleNotes(false)).toEqual(['A#', 'B#', 'C#', 'D#', 'E#', 'F#', 'G##'])
  })

  it('renders the scale name', () => {
    expect(cMajor.name(false)).toEqual('C major')
    expect(cSharpMinor.name(false)).toEqual('C# natural minor')
  })
})

describe('Key scale degree', () => {
  it('have a note', () => {
    expect(cMajor.degrees(false)[0].note).toEqual('C')
    expect(fMajor.degrees(false)[3].note).toEqual('Bb')
    expect(cSharpMinor.degrees(false)[6].note).toBe('B')
  })
})

describe('Key scale chord - enharmonic sharp', () => {
  it('renders triad chord symbol', () => {
    expect(cMajor.degrees(false)[0].chords.triad.chordSymbol).toEqual('C')
    expect(cMajor.degrees(false)[1].chords.triad.chordSymbol).toEqual('Dm')
    expect(cMajor.degrees(false)[6].chords.triad.chordSymbol).toEqual('B<sup>o</sup>')
    
    expect(fMajor.degrees(false)[3].chords.triad.chordSymbol).toEqual('Bb')
    
    expect(cSharpMinor.degrees(false)[0].chords.triad.chordSymbol).toEqual('C#m')
    expect(cSharpMinor.degrees(false)[1].chords.triad.chordSymbol).toEqual('D#<sup>o</sup>')

    expect(aSharpHarmMinor.degrees(false)[2].chords.triad.chordSymbol).toEqual('C#<sup>+</sup>')
    expect(aSharpHarmMinor.degrees(false)[6].chords.triad.chordSymbol).toEqual('G##<sup>o</sup>')
  })

  it('renders seventh chord symbol', () => {
    expect(cMajor.degrees(false)[0].chords.seventh.chordSymbol).toEqual('C<sup>maj7</sup>')
    expect(cMajor.degrees(false)[1].chords.seventh.chordSymbol).toEqual('Dm<sup>7</sup>')
    expect(cMajor.degrees(false)[6].chords.seventh.chordSymbol).toEqual('B<sup>ø</sup>')
    
    expect(fMajor.degrees(false)[3].chords.seventh.chordSymbol).toEqual('Bb<sup>maj7</sup>')
    
    expect(cSharpMinor.degrees(false)[0].chords.seventh.chordSymbol).toEqual('C#m<sup>7</sup>')
    expect(cSharpMinor.degrees(false)[1].chords.seventh.chordSymbol).toEqual('D#<sup>ø</sup>')

    expect(aSharpHarmMinor.degrees(false)[2].chords.seventh.chordSymbol).toEqual('C#+<sup>M7</sup>')
    expect(aSharpHarmMinor.degrees(false)[6].chords.seventh.chordSymbol).toEqual('G##<sup>o7</sup>')
  })

  it('returns correct notes for triads', () => {
    expect(cMajor.degrees(false)[0].chords.triad.notes(3, 0)).toEqual(['C3', 'E3', 'G3'])
    expect(cMajor.degrees(false)[1].chords.triad.notes(3, 0)).toEqual(['D3', 'F3', 'A3'])
    expect(cMajor.degrees(false)[6].chords.triad.notes(3, 0)).toEqual(['B3', 'D4', 'F4'])
    
    expect(fMajor.degrees(false)[3].chords.triad.notes(3, 0)).toEqual(['A#3', 'D4', 'F4'])
    
    expect(cSharpMinor.degrees(false)[0].chords.triad.notes(3, 0)).toEqual(['C#3', 'E3', 'G#3'])
    expect(cSharpMinor.degrees(false)[1].chords.triad.notes(3, 0)).toEqual(['D#3', 'F#3', 'A3'])
    expect(cSharpMinor.degrees(false)[5].chords.triad.notes(3, 0)).toEqual(['A3', 'C#4', 'E4'])

    expect(aSharpHarmMinor.degrees(false)[2].chords.triad.notes(3, 0)).toEqual(['C#4', 'F4', 'A4'])
    expect(aSharpHarmMinor.degrees(false)[6].chords.triad.notes(3, 0)).toEqual(['A4', 'C5', 'D#5'])
  })

  it('returns correct notes for sevenths', () => {
    expect(cMajor.degrees(false)[0].chords.seventh.notes(3, 0)).toEqual(['C3', 'E3', 'G3', 'B3'])
    expect(cMajor.degrees(false)[1].chords.seventh.notes(3, 0)).toEqual(['D3', 'F3', 'A3', 'C4'])
    expect(cMajor.degrees(false)[6].chords.seventh.notes(3, 0)).toEqual(['B3', 'D4', 'F4', 'A4'])
    
    expect(fMajor.degrees(false)[3].chords.seventh.notes(3, 0)).toEqual(['A#3', 'D4', 'F4', 'A4'])
    
    expect(cSharpMinor.degrees(false)[0].chords.seventh.notes(3, 0)).toEqual(['C#3', 'E3', 'G#3', 'B3'])
    expect(cSharpMinor.degrees(false)[1].chords.seventh.notes(3, 0)).toEqual(['D#3', 'F#3', 'A3', 'C#4'])
    expect(cSharpMinor.degrees(false)[5].chords.seventh.notes(3, 0)).toEqual(['A3', 'C#4', 'E4', 'G#4'])

    expect(aSharpHarmMinor.degrees(false)[2].chords.seventh.notes(3, 0)).toEqual(['C#4', 'F4', 'A4', 'C5'])
    expect(aSharpHarmMinor.degrees(false)[6].chords.seventh.notes(3, 0)).toEqual(['A4', 'C5', 'D#5', 'F#5'])
  })

  it('returns inversions for triads', () => {
    expect(cMajor.degrees(false)[0].chords.triad.notes(3, 1)).toEqual(['E3', 'G3', 'C4'])
    expect(cMajor.degrees(false)[0].chords.triad.notes(3, 2)).toEqual(['G3', 'C4', 'E4'])

    expect(cMajor.degrees(false)[6].chords.triad.notes(3, 1)).toEqual(['D4', 'F4', 'B4'])
    expect(cMajor.degrees(false)[6].chords.triad.notes(3, 2)).toEqual(['F4', 'B4', 'D5'])
  })

  it('returns inversions of seventh chords', () => {
    expect(cMajor.degrees(false)[0].chords.seventh.notes(3, 1)).toEqual(['E3', 'G3', 'B3', 'C4'])
    expect(cMajor.degrees(false)[0].chords.seventh.notes(3, 2)).toEqual(['G3', 'B3', 'C4', 'E4'])
    expect(cMajor.degrees(false)[0].chords.seventh.notes(3, 3)).toEqual(['B3', 'C4', 'E4', 'G4'])

    expect(cMajor.degrees(false)[6].chords.seventh.notes(3, 1)).toEqual(['D4', 'F4', 'A4', 'B4'])
    expect(cMajor.degrees(false)[6].chords.seventh.notes(3, 2)).toEqual(['F4', 'A4', 'B4', 'D5'])
    expect(cMajor.degrees(false)[6].chords.seventh.notes(3, 3)).toEqual(['A4', 'B4', 'D5', 'F5'])
  })
})

describe('Key scale chord - enharmonic flat', () => {
  it('renders triad chord symbol', () => {
    expect(cMajor.degrees(true)[0].chords.triad.chordSymbol).toEqual('C')
    expect(cMajor.degrees(true)[1].chords.triad.chordSymbol).toEqual('Dm')
    expect(cMajor.degrees(true)[6].chords.triad.chordSymbol).toEqual('B<sup>o</sup>')
    
    expect(fMajor.degrees(true)[3].chords.triad.chordSymbol).toEqual('Bb')
    
    expect(cSharpMinor.degrees(true)[0].chords.triad.chordSymbol).toEqual('Dbm')
    expect(cSharpMinor.degrees(true)[1].chords.triad.chordSymbol).toEqual('Eb<sup>o</sup>')

    expect(aSharpHarmMinor.degrees(true)[0].chords.triad.chordSymbol).toEqual('Bbm')
    expect(aSharpHarmMinor.degrees(true)[2].chords.triad.chordSymbol).toEqual('Db<sup>+</sup>')
    expect(aSharpHarmMinor.degrees(true)[6].chords.triad.chordSymbol).toEqual('A<sup>o</sup>')
  })

  it('renders seventh chord symbol', () => {
    expect(cMajor.degrees(true)[0].chords.seventh.chordSymbol).toEqual('C<sup>maj7</sup>')
    expect(cMajor.degrees(true)[1].chords.seventh.chordSymbol).toEqual('Dm<sup>7</sup>')
    expect(cMajor.degrees(true)[6].chords.seventh.chordSymbol).toEqual('B<sup>ø</sup>')
    
    expect(fMajor.degrees(true)[3].chords.seventh.chordSymbol).toEqual('Bb<sup>maj7</sup>')
    
    expect(cSharpMinor.degrees(true)[0].chords.seventh.chordSymbol).toEqual('Dbm<sup>7</sup>')
    expect(cSharpMinor.degrees(true)[1].chords.seventh.chordSymbol).toEqual('Eb<sup>ø</sup>')

    expect(aSharpHarmMinor.degrees(true)[2].chords.seventh.chordSymbol).toEqual('Db+<sup>M7</sup>')
    expect(aSharpHarmMinor.degrees(true)[6].chords.seventh.chordSymbol).toEqual('A<sup>o7</sup>')
  })

  it('returns correct notes for triads', () => {
    expect(aSharpHarmMinor.degrees(true)[6].chords.triad.notes(3, 0)).toEqual(['A4', 'C5', 'D#5'])
  })

  it('returns correct notes for sevenths', () => {
    expect(aSharpHarmMinor.degrees(true)[6].chords.seventh.notes(3, 0)).toEqual(['A4', 'C5', 'D#5', 'F#5'])
  })

  it('returns inversions for triads', () => {
    expect(cMajor.degrees(true)[0].chords.triad.notes(3, 1)).toEqual(['E3', 'G3', 'C4'])
    expect(cMajor.degrees(true)[0].chords.triad.notes(3, 2)).toEqual(['G3', 'C4', 'E4'])

    expect(cMajor.degrees(true)[6].chords.triad.notes(3, 1)).toEqual(['D4', 'F4', 'B4'])
    expect(cMajor.degrees(true)[6].chords.triad.notes(3, 2)).toEqual(['F4', 'B4', 'D5'])
  })

  it('returns inversions of seventh chords', () => {
    expect(cMajor.degrees(true)[0].chords.seventh.notes(3, 1)).toEqual(['E3', 'G3', 'B3', 'C4'])
    expect(cMajor.degrees(true)[0].chords.seventh.notes(3, 2)).toEqual(['G3', 'B3', 'C4', 'E4'])
    expect(cMajor.degrees(true)[0].chords.seventh.notes(3, 3)).toEqual(['B3', 'C4', 'E4', 'G4'])

    expect(cMajor.degrees(true)[6].chords.seventh.notes(3, 1)).toEqual(['D4', 'F4', 'A4', 'B4'])
    expect(cMajor.degrees(true)[6].chords.seventh.notes(3, 2)).toEqual(['F4', 'A4', 'B4', 'D5'])
    expect(cMajor.degrees(true)[6].chords.seventh.notes(3, 3)).toEqual(['A4', 'B4', 'D5', 'F5'])
  })
})