import { ChordProgressionChord } from "@/application/models/chordProgression"
import { TRIAD, getVariant, SEVENTH } from '@/application/models/chords'
import { KeyScaleChord } from '@/application/models/ScaleInKey'

const triad = getVariant<KeyScaleChord>(TRIAD)
const seventh = getVariant<KeyScaleChord>(SEVENTH)

describe('Chord progression chord', () => {
  it('turns a chord into a string representation and back', () => {
    function check(chord: ChordProgressionChord) {
      const asQuery = chord.chordToQuery()
      const asChord = ChordProgressionChord.chordFromQuery(asQuery)
      expect(JSON.stringify(asChord)).toEqual(JSON.stringify(chord))
    }
    
    check(new ChordProgressionChord(1, triad, 0, 2))
    check(new ChordProgressionChord(5, triad, 2, 4))
    check(new ChordProgressionChord(2, seventh, 1, 3))
    check(new ChordProgressionChord(6, seventh, 3, 2))
  })

  it('returns undefined if chord string does not match pattern', () => {
    function check(str: string) {
      expect(ChordProgressionChord.chordFromQuery(str)).toBeUndefined()
    }
    
    check('')
    check('12364')
    check('r')
    
    // chord variant doesn't match 3 or 7
    check('1.4.0.2')
    // triad with an invalid inversion
    check('1.3.3.2')
    // unsupported octave
    check('1.3.0.5')
    // degree doesn't exist
    check('0.3.0.2')
  })

  it('turns a set of chords into a query parameter', () => {
    const chords = [
      new ChordProgressionChord(1, triad, 0, 2),
      undefined,
      new ChordProgressionChord(2, seventh, 1, 3),
      undefined
    ]
    const query = ChordProgressionChord.toQuery(chords)
    expect(query).toEqual('1.3.0.2;r;2.7.1.3')
  })

  it('turns a query parameter into a set of chords', () => {
    const query = '1.3.0.2;r;2.7.1.3;r'
    const expected = [
      new ChordProgressionChord(1, triad, 0, 2),
      undefined,
      new ChordProgressionChord(2, seventh, 1, 3)
    ]
    const result = ChordProgressionChord.fromQuery(query)
    expect(JSON.stringify(result)).toEqual(JSON.stringify(expected))
  })

  it('returns empty list if no chords are found', () => {
    expect(ChordProgressionChord.fromQuery('')).toEqual([])
    expect(ChordProgressionChord.fromQuery(undefined)).toEqual([])
    expect(ChordProgressionChord.fromQuery('r')).toEqual([])
    expect(ChordProgressionChord.fromQuery('r;r;r')).toEqual([])
    expect(ChordProgressionChord.fromQuery('8.0.2.9')).toEqual([])
  })

  it('returns null when chords are empty', () => {
    expect(ChordProgressionChord.toQuery([])).toBeNull()
    expect(ChordProgressionChord.toQuery([undefined])).toBeNull()
    expect(ChordProgressionChord.toQuery([undefined, undefined])).toBeNull()
  })
})